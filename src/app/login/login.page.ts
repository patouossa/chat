import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../services/auth-service.service";
import {AngularFireAuth} from "@angular/fire/auth";
import {LoadingController, ToastController} from "@ionic/angular";
import {Router} from "@angular/router";
import {FirebaseService} from "../services/firebase.service";

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    validations_form: FormGroup;
    errorMessage: string = '';

    validation_messages = {
        'email': [
            {type: 'required', message: 'L\'email est requis.'},
            {type: 'pattern', message: 'Veuillez saisir un e-mail valide.'}
        ],
        'password': [
            {type: 'required', message: 'Le mot de passe est requis.'},
            {type: 'minlength', message: 'Le mot de passe doit comporter au moins 5 caractères.'}
        ]
    };

    constructor(private authService: AuthService,
                private formBuilder: FormBuilder,
                public afAuth: AngularFireAuth,
                public firebaseService: FirebaseService,
                public toastController: ToastController,
                public loadingController: LoadingController,
                private router: Router) {
        if (localStorage.getItem("user") != null) {
            this.router.navigate(['/chats']);
        }
    }

    ngOnInit() {
        this.validations_form = this.formBuilder.group({
            email: new FormControl('', Validators.compose([
                Validators.required,
                Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            password: new FormControl('', Validators.compose([
                Validators.minLength(5),
                Validators.required
            ])),
        });
    }

    async tryLogin(value) {
        const loading = await this.loadingController.create({
            message: 'Authentification en cours'
        });
        loading.present();

        const $this = this;
        this.authService.webLogin(value)
            .subscribe(res => {
                console.log(res);
                loading.dismiss();
                if (res.result != null) {
                    localStorage.setItem("user", JSON.stringify(res.result));
                    $this.router.navigate(['/chats']);
                } else {
                    this.errorMessage = "Erreur d'authentification, veuillez vérifier vos infos!";
                }

            }, err => {
                loading.dismiss();
                this.errorMessage = err.message;
                console.log(err);
            });
    }

    open(){
        window.open("https://spowatcher.com/aristoka/feed/signup.php",'_system', 'location=yes');
    }

    goRegisterPage() {
        this.router.navigate(['/register']);
    }

}
