import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {AngularFireModule} from "@angular/fire";
import {environment} from "../environments/environment";
import {AngularFireAuthModule} from "@angular/fire/auth";
import {AngularFireDatabaseModule} from "@angular/fire/database";
import {AngularFirestore, AngularFirestoreModule, SETTINGS} from "@angular/fire/firestore";
import {AngularFireStorageModule} from "@angular/fire/storage";
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import {Camera} from '@ionic-native/camera/ngx';
//import {ConnectionBackend, Http, HttpModule} from "@angular/http";
import {HttpClientModule} from "@angular/common/http";
import { HTTP } from '@ionic-native/http/ngx';
import {ImagePicker} from "@ionic-native/image-picker/ngx";
import {IonicImageLoader} from "ionic-image-loader";
import {WebView} from '@ionic-native/ionic-webview/ngx';

@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [
        BrowserModule,
        HttpClientModule,
        IonicModule.forRoot(),

        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFireDatabaseModule,
        AngularFirestoreModule,
        AngularFireStorageModule,
        IonicImageLoader.forRoot(),
        //HttpModule,
        AppRoutingModule
    ],
    providers: [
        StatusBar,
        SplashScreen,
        AngularFirestore,
        LocalNotifications,
        ImagePicker,
        Camera,
        WebView,
        BackgroundMode,
        HTTP,
        {provide: SETTINGS, useValue: {}},
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
