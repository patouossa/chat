import { Injectable } from "@angular/core";

// @ts-ignore
import * as firebase from "firebase";
import { FirebaseService } from './firebase.service';
import { AngularFireAuth } from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';
import 'firebase/storage';
import {environment} from '../../environments/environment';
import App = firebase.app.App;
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import { HTTP } from '@ionic-native/http/ngx';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
      private firebaseService: FirebaseService,
      public afs: AngularFirestore,
      private http: HttpClient,
      public afAuth: AngularFireAuth
  ){}

  findApp(name){
    let r_app = false;
    firebase.apps.forEach(app=> {
      if(app.name == name)
        r_app = true;
    });
    return r_app;
  }

  doRegister(value){
    return new Promise<any>((resolve, reject) => {
      var authApp = (firebase.apps.length > 0 && this.findApp("authApp"))? firebase.app("authApp") : firebase.initializeApp(environment.firebase, 'authApp');
      var detachedAuth = authApp.auth();

      //detachedAuth.createUserWithEmailAndPassword('foo@example.com', 'asuperrandompassword');
      //firebase.auth()
      detachedAuth.createUserWithEmailAndPassword(value.email, value.password)
          .then(
              res => resolve(res),
              err => reject(err))
    })
  }

  saveUser(value, id, utype){
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      console.log(currentUser);
      if(id == null){

        value.password = this.makeid(8);

        this.doRegister(value)
            .then(result => {
              console.log(result);
              this.afs.collection('users').add({
                id : result.user.uid,
                name: value.name,
                email: value.email,
                tel: value.tel,
                specialite: value.speciality,
                password: value.password,
                utype: utype,
                creator: currentUser.uid,
                creator_name: JSON.parse(localStorage.getItem("user")).name,
                address: value.address
              })
                  .then(
                      res => resolve(res),
                      err => reject(err)
                  )

            }, err => {
              console.log(err);
              reject(err);
            });

      }
      else{
        this.afs.collection('users').doc(id).update({
          name: value.name,
          tel: value.tel,
          specialite: value.speciality,
          address: value.address
        })
            .then(
                res => resolve(res),
                err => reject(err)
            )
      }

    })
  }

  webLogin(value): Observable<any> {
    let headerDict: any = {
      'Content-type': 'application/json'
    };
    return this.http.get("http://chat.vr-house.org/login.php?email="+value.email+"&password="+value.password, {headers: new HttpHeaders(headerDict)});
  }

  doLogin(value){
    return new Promise<any>((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(value.email, value.password)
          .then(
              res => resolve(res),
              err => reject(err))
    })
  }

  doLogout(){
    return new Promise((resolve, reject) => {
      // @ts-ignore
      firebase.auth().signOut()
          .then(() => {
            this.firebaseService.unsubscribeOnLogOut();
            resolve();
          }).catch((error) => {
            console.log(error);
        reject();
      });
    })
  }

  makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
}
