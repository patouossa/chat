import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
// @ts-ignore
import * as firebase from "firebase/app";
import 'firebase/storage';
import {AngularFireAuth} from '@angular/fire/auth';
import {ToastController} from "@ionic/angular";
import {Observable} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import { HTTP } from '@ionic-native/http/ngx';

@Injectable({
    providedIn: 'root'
})
export class FirebaseService {

    private snapshotChangesSubscription: any;

    constructor(
        public afs: AngularFirestore,
        public toastController: ToastController,
        private http: HttpClient,
        public afAuth: AngularFireAuth
    ) {
    }



    getFriends(id): Observable<any> {
        let headerDict: any = {
            'Content-type': 'application/json'
        };
        return this.http.get("http://chat.vr-house.org/get_friends.php?id="+id, {headers: new HttpHeaders(headerDict)});
    }

    getUser() {
        return new Promise<any>((resolve, reject) => {
            this.afAuth.user.subscribe(currentUser => {
                console.log(currentUser);
                if (currentUser) {
                    this.snapshotChangesSubscription = this.afs.collection('users', ref => ref.where('id', '==', currentUser.uid)).valueChanges({idField: 'dataId'})
                        .subscribe(snapshots => {
                            console.log(snapshots);
                            //resolve(snapshots);
                            if (snapshots.length == 1) {
                                localStorage.setItem("user", JSON.stringify(snapshots[0]));
                                resolve(snapshots[0]);
                            } else {
                                resolve(null);
                            }
                        }, err => {
                            reject(err);
                        });
                    //resolve(this.snapshotChangesSubscription);
                }
            });
        });
    }


    getUserById(id) {
        return new Promise<any>((resolve, reject) => {
            this.snapshotChangesSubscription = this.afs.collection('users').doc(id).valueChanges()
                .subscribe(snapshots => {
                    resolve(snapshots);
                }, err => {
                    reject(err);
                });
        });
    }



    getAllPatientsByField(table, field, value) {
        return new Promise<any>((resolve, reject) => {
            this.snapshotChangesSubscription = this.afs.collection(table, ref => ref.where(field, '==', value).where("utype", "==", "PATIENT")).valueChanges({idField: 'dataId'})
                .subscribe(snapshots => {
                    resolve(snapshots);
                }, err => {
                    reject(err);
                });
        });
    }

    getDataById(table, id) {
        return new Promise<any>((resolve, reject) => {
            this.afAuth.user.subscribe(currentUser => {
                if (currentUser) {
                    this.snapshotChangesSubscription = this.afs.collection(table).doc(id).valueChanges()
                        .subscribe(snapshots => {
                            resolve(snapshots);
                        }, err => {
                            reject(err);
                        });
                    //resolve(this.snapshotChangesSubscription);
                }
            });
        });
    }

    getSubDataById(table1, table2, id1, id2) {
        return new Promise<any>((resolve, reject) => {
            this.afAuth.user.subscribe(currentUser => {
                if (currentUser) {
                    this.snapshotChangesSubscription = this.afs.collection(table1).doc(id1).collection(table2).doc(id2).valueChanges()
                        .subscribe(snapshots => {
                            resolve(snapshots);
                        }, err => {
                            reject(err);
                        });
                    //resolve(this.snapshotChangesSubscription);
                }
            });
        });
    }

    getTasks() {
        return new Promise<any>((resolve, reject) => {
            this.afAuth.user.subscribe(currentUser => {
                if (currentUser) {
                    this.snapshotChangesSubscription = this.afs.collection('tasks', ref => ref.where('user_id', '==', currentUser.uid)).snapshotChanges();
                    resolve(this.snapshotChangesSubscription);
                }
            });
        });
    }

    getAlldataCreatedByCurrentUser(table) {
        return new Promise<any>((resolve, reject) => {
            this.afAuth.user.subscribe(currentUser => {
                if (currentUser) {
                    this.snapshotChangesSubscription = this.afs.collection(table, ref => ref.where('user_id', '==', currentUser.uid)).snapshotChanges();
                    resolve(this.snapshotChangesSubscription);
                }
            });
        });
    }

    getAllTaskByProject(project_id) {
        return new Promise<any>((resolve, reject) => {
            this.snapshotChangesSubscription = this.afs.collection('tasks', ref => ref.where('project_id', '==', project_id)).valueChanges({idField: 'dataId'})
                .subscribe(snapshots => {
                    resolve(snapshots);
                }, err => {
                    reject(err);
                });
        });
    }

    getAllTaskByUser(user_id) {
        return new Promise<any>((resolve, reject) => {
            this.snapshotChangesSubscription = this.afs.collection('tasks', ref => ref.where('user_id', '==', user_id)).valueChanges({idField: 'dataId'})
                .subscribe(snapshots => {
                    resolve(snapshots);
                }, err => {
                    reject(err);
                });
        });
    }

    getAlldataByField(table, field, value) {
        return new Promise<any>((resolve, reject) => {
            this.snapshotChangesSubscription = this.afs.collection(table, ref => ref.where(field, '==', value)).valueChanges({idField: 'dataId'})
                .subscribe(snapshots => {
                    resolve(snapshots);
                }, err => {
                    reject(err);
                });
        });
    }

    getAlldata(table) {
        return new Promise<any>((resolve, reject) => {
            this.snapshotChangesSubscription = this.afs.collection(table).valueChanges({idField: 'dataId'})
                .subscribe(snapshots => {
                    resolve(snapshots);
                }, err => {
                    reject(err);
                });
        });
    }

    getAllSubData(table1, id, table2) {
        return new Promise<any>((resolve, reject) => {
            this.snapshotChangesSubscription = this.afs.collection(table1).doc(id).collection(table2).valueChanges({idField: 'dataId'})
                .subscribe(snapshots => {
                    resolve(snapshots);
                }, err => {
                    reject(err);
                });
        });
    }

    deleteData(table, id) {
        return new Promise<any>((resolve, reject) => {
            let currentUser = firebase.auth().currentUser;
            this.afs.collection(table).doc(id).delete()
                .then(
                    res => resolve(res),
                    err => reject(err)
                );
        });
    }

    getTask(taskId) {
        return new Promise<any>((resolve, reject) => {
            this.afAuth.user.subscribe(currentUser => {
                if (currentUser) {
                    this.snapshotChangesSubscription = this.afs.doc<any>('tasks/' + taskId).valueChanges()
                        .subscribe(snapshots => {
                            resolve(snapshots);
                        }, err => {
                            reject(err);
                        });
                }
            });
        });
    }

    createOrdonnance(value, id) {
        return new Promise<any>((resolve, reject) => {
            let currentUser = firebase.auth().currentUser;
            console.log(currentUser);
            if(id == null){
                this.afs.collection('ordonnances').add({
                    id: this.generator(),
                    patient_id: value.patient_id,
                    patient_name: value.patient_name,
                    creator_id: value.creator_id,
                    creator_name: value.creator_name,
                    observations: value.observations,
                    date: new Date().toLocaleString(),
                    drugs: value.drugs
                })
                    .then(
                        res => resolve(res),
                        err => reject(err)
                    );
            }
            else{
                this.afs.collection('projects').doc(id).update({
                    id: this.generator(),
                    name: value.name,
                    image: value.image,
                    description: value.description
                })
                    .then(
                        res => resolve(res),
                        err => reject(err)
                    );
            }

        });
    }

    createTask(value, assigned_date, date_completed, task_id, id) {
        return new Promise<any>((resolve, reject) => {
            let currentUser = firebase.auth().currentUser;
            console.log(currentUser);
            if (id == null) {
                if (task_id == null) {
                    this.afs.collection('tasks').add({
                        project_name: value.project_name,
                        user_name: value.user_name,
                        project_id: value.project_id,
                        user_id: value.user_id,
                        status: value.status,
                        assigned_date: assigned_date,
                        priority: value.priority,
                        delivrables: value.delivrables,
                        deadline: value.deadline,
                        date_completed: date_completed,
                        description: value.description,
                        task_id: task_id
                    })
                        .then(
                            res => resolve(res),
                            err => reject(err)
                        );
                } else {
                    this.afs.collection('tasks').doc(task_id).collection('subtasks').add({
                        project_name: value.project_name,
                        user_name: value.user_name,
                        project_id: value.project_id,
                        user_id: value.user_id,
                        status: value.status,
                        assigned_date: assigned_date,
                        priority: value.priority,
                        delivrables: value.delivrables,
                        deadline: value.deadline,
                        date_completed: date_completed,
                        description: value.description,
                        task_id: task_id
                    })
                        .then(
                            res => resolve(res),
                            err => reject(err)
                        );
                }

            } else {
                if (task_id == null) {
                    this.afs.collection('tasks').doc(id).set({
                        project_name: value.project_name,
                        user_name: value.user_name,
                        project_id: value.project_id,
                        user_id: value.user_id,
                        status: value.status,
                        assigned_date: assigned_date,
                        priority: value.priority,
                        delivrables: value.delivrables,
                        deadline: value.deadline,
                        date_completed: date_completed,
                        description: value.description,
                        task_id: task_id
                    })
                        .then(
                            res => resolve(res),
                            err => reject(err)
                        );
                } else {
                    this.afs.collection('tasks').doc(task_id).collection('subtasks').doc(id).set({
                        project_name: value.project_name,
                        user_name: value.user_name,
                        project_id: value.project_id,
                        user_id: value.user_id,
                        status: value.status,
                        assigned_date: assigned_date,
                        priority: value.priority,
                        delivrables: value.delivrables,
                        deadline: value.deadline,
                        date_completed: date_completed,
                        description: value.description,
                        task_id: task_id
                    })
                        .then(
                            res => resolve(res),
                            err => reject(err)
                        );
                }

            }

        });
    }

    private generator(): string {
        const isString = `${this.S4()}${this.S4()}-${this.S4()}-${this.S4()}-${this.S4()}-${this.S4()}${this.S4()}${this.S4()}`;

        return isString;
    }

    private S4(): string {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }

    unsubscribeOnLogOut() {
        //remember to unsubscribe from the snapshotChanges
        if (this.snapshotChangesSubscription) {
            this.snapshotChangesSubscription.unsubscribe();
        }
    }


    encodeImageUri(imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext('2d');
        var img = new Image();
        img.onload = function() {
            var aux: any = this;
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            var dataURL = c.toDataURL('image/jpeg');
            callback(dataURL);
        };
        img.src = imageUri;
    };

    uploadImage(imageURI, randomId) {
        return new Promise<any>((resolve, reject) => {
            let storageRef = firebase.storage().ref();
            let imageRef = storageRef.child('image').child(randomId);
            this.encodeImageUri(imageURI, function(image64) {
                imageRef.putString(image64, 'data_url')
                    .then(snapshot => {
                        snapshot.ref.getDownloadURL()
                            .then(res => resolve(res));
                    }, err => {
                        reject(err);
                    });
            });
        });
    }


    checkeDrId(id, list){
        let is_present = false;
        list.forEach(d => {
            if(d.user_id == id)
                is_present = true;
        })
        return is_present;
    }

    getPatientChats(me) {
        return new Promise<any>((resolve, reject) => {
            this.afAuth.user.subscribe(currentUser => {
                console.log(currentUser);
                if (currentUser) {
                    this.snapshotChangesSubscription = this.afs.collection('ordonnances', ref => ref.where('patient_id', '==', currentUser.uid)).valueChanges({idField: 'dataId'})
                        .subscribe(snapshots => {
                            var doctors = [];
                            doctors.push({user_id: me.creator, user_name: me.creator_name});
                            snapshots.forEach((ord :any) => {
                               if(!this.checkeDrId(ord.creator_id, doctors)){
                                   doctors.push({user_id: ord.creator_id, user_name: ord.creator_name})
                               }
                            });
                            console.log(snapshots);
                            resolve(doctors);
                        }, err => {
                            reject(err);
                        });
                    //resolve(this.snapshotChangesSubscription);
                }
            });
        });
    }

    getDoctorChats(me) {
        return new Promise<any>((resolve, reject) => {
            this.afAuth.user.subscribe(currentUser => {
                console.log(currentUser);
                if (currentUser) {
                    this.snapshotChangesSubscription = this.afs.collection('ordonnances', ref => ref.where('creator_id', '==', currentUser.uid)).valueChanges({idField: 'dataId'})
                        .subscribe(snapshots => {
                            var patients = [];
                            //patients.push({user_id: me.creator, user_name: me.creator_name});
                            snapshots.forEach((ord :any) => {
                               if(!this.checkeDrId(ord.patient_id, patients)){
                                   patients.push({user_id: ord.patient_id, user_name: ord.patient_name})
                               }
                            });
                            console.log(snapshots);
                            resolve(patients);
                        }, err => {
                            reject(err);
                        });
                    //resolve(this.snapshotChangesSubscription);
                }
            });
        });
    }


    getMessages(me, his_id) {
        return new Promise<any>((resolve, reject) => {
            this.afAuth.user.subscribe(currentUser => {
                console.log(currentUser);
                if (currentUser) {
                    this.snapshotChangesSubscription = this.afs.collection('chat', ref => ref.orderBy('date_stamp', 'desc')).valueChanges({idField: 'dataId'})
                        .subscribe(snapshots => {
                            var chats = [];
                            snapshots.forEach((c :any) => {
                               if((c.from_id == me.id && c.to_id == his_id) || (c.from_id == his_id && c.to_id == me.id)){
                                   chats.push(c);
                               }
                            });
                            console.log(snapshots);
                            resolve(chats);
                        }, err => {
                            reject(err);
                        });
                    //resolve(this.snapshotChangesSubscription);
                }
            });
        });
    }


    sendMessage(value, id) {
        return new Promise<any>((resolve, reject) => {
            let currentUser = firebase.auth().currentUser;
            console.log(currentUser);
            if(id == null){
                this.afs.collection('chat').add({
                    id: this.generator(),
                    to_id: value.to_id,
                    to_name: value.to_name,
                    from_name: value.from_name,
                    from_id: value.from_id,
                    message: value.message,
                    type: value.type,
                    is_new: true,
                    date: new Date().toLocaleString(),
                    date_stamp: Date.now()
                })
                    .then(
                        res => resolve(res),
                        err => reject(err)
                    );
            }

        });
    }

    public async  creatToast(message, color="danger"){
        const toast = await this.toastController.create({
            message: message,
            color: color,
            duration: 2000
        });
        toast.present();
    }
}
