import {Component} from '@angular/core';

import {LoadingController, NavController, Platform, ToastController} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {AuthService} from "./services/auth-service.service";
import {FirebaseService} from "./services/firebase.service";
import {AngularFireAuth} from "@angular/fire/auth";
import {ActivatedRoute, Router} from "@angular/router";
import {AngularFirestore} from "@angular/fire/firestore";
import {LocalNotifications} from '@ionic-native/local-notifications/ngx';

import { BackgroundMode } from '@ionic-native/background-mode/ngx';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent {
    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private authService: AuthService,
        public firebase: FirebaseService,
        public afAuth: AngularFireAuth,
        public toastController: ToastController,
        public loadingController: LoadingController,
        private router: Router,
        public afs: AngularFirestore,
        private activatedRoute: ActivatedRoute,
        private localNotifications: LocalNotifications,
        public navController: NavController,
        public backgroundMode: BackgroundMode
    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
        const $this = this;
        this.backgroundMode.setDefaults({ silent: true });
        this.backgroundMode.enable();
        this.backgroundMode.on('activate').subscribe( ()=>{
            console.log("background mode started");
            $this.getNewMessages();
        });
    }


    getNewMessages() {
        const user = JSON.parse(localStorage.getItem("user"));
        this.afs.collection('chat', ref => ref.where('is_new', "==", true).where("to_id", "==", user.id)).valueChanges({idField: 'dataId'})
            .subscribe((snapshots):any => {
                console.log(snapshots);

                if (snapshots.length > 0) {

                }
                const notif = [];
                let u_id = null;
                let pseudo = "";
                snapshots.forEach((c: any) => {
                    notif.push({
                        title: "Message de " + c.from_name,
                        text: c.message,
                        foreground: true
                    });
                    pseudo = c.from_name

                    u_id = c.from_id;

                    this.afs.collection('chat').doc(c.dataId).update({
                        is_new: false
                    })
                        .then(
                            res => {
                                console.log("set viewed correctly");
                            },
                            err => {
                                console.log("error setting viewed ", err);
                            }
                        );
                });

                if (snapshots.length > 0) {
                    this.localNotifications.schedule(notif);
                    this.localNotifications.on("click").subscribe(d => {
                        this.router.navigate(["/chatting"], {queryParams: {id: u_id, pseudo: pseudo}});
                    });
                }
            }, err => {
                //reject(err);
            });


        /*this.firebase.getMessages(this.me, this.his_id).then(res => {
            this.messages = res;
        });*/
    }
}
