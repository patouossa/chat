import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../services/auth-service.service";
import {FirebaseService} from "../../services/firebase.service";
import {AngularFireAuth} from "@angular/fire/auth";
import {LoadingController, ToastController} from "@ionic/angular";
import {Router} from "@angular/router";

@Component({
    selector: 'app-chats',
    templateUrl: './chats.page.html',
    styleUrls: ['./chats.page.scss'],
})
export class ChatsPage implements OnInit {

    chats = [];
    user = null;

    constructor(
        private authService: AuthService,
        public firebase: FirebaseService,
        public afAuth: AngularFireAuth,
        public toastController: ToastController,
        public loadingController: LoadingController,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem("user"));
        this.loadChats();
    }


    async loadChats() {
        const loading = await this.loadingController.create({
            message: 'Chargement...'
        });
        loading.present();
        this.firebase.getFriends(this.user.id)
            .subscribe(res => {
                this.chats = res.result == null ? [] : res.result;
                console.log(this.chats);
                loading.dismiss();
            }, err => {
                this.firebase.creatToast(err);
                loading.dismiss();
            });

    }

    JStringify(obj){
      return JSON.stringify(obj);
    }


    logout(){
        localStorage.removeItem("user");
        this.router.navigate(['/login']);
    }

    open() {
        window.open("https://spowatcher.com/aristoka/feed/login.php",'_system', 'location=yes');
    }
}
