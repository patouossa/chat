import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthService} from "../../services/auth-service.service";
import {FirebaseService} from "../../services/firebase.service";
import {AngularFireAuth} from "@angular/fire/auth";
import {LoadingController, NavController, ToastController, ActionSheetController} from "@ionic/angular";
import {ActivatedRoute, Router} from "@angular/router";
import {AngularFirestore} from "@angular/fire/firestore";
import {Camera, CameraOptions} from "@ionic-native/camera/ngx";
import {PictureSourceType} from "@ionic-native/camera";

import * as firebase from "firebase/app";
import 'firebase/storage';
import {ImagePicker} from "@ionic-native/image-picker/ngx";

@Component({
    selector: 'app-chatting',
    templateUrl: './chatting.page.html',
    styleUrls: ['./chatting.page.scss'],
})
export class ChattingPage implements OnInit {
    @ViewChild('content') private content: any;
    loading = "";
    name = null;
    messages = [];
    msgVal = '';
    his_id = null;
    him = null;

    me = null;



    options: CameraOptions = {
        quality: 30,
        //allowEdit: true,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
    };

    constructor(
        private authService: AuthService,
        private actionSheetCtr: ActionSheetController,
        public fireb: FirebaseService,
        public afAuth: AngularFireAuth,
        public toastController: ToastController,
        public loadingController: LoadingController,
        private router: Router,
        private camera: Camera,
        public imagePicker: ImagePicker,
        public afs: AngularFirestore,
        private activatedRoute: ActivatedRoute,
        public navController: NavController
    ) {
    }

    ngOnInit() {
        this.me = JSON.parse(localStorage.getItem("user"));
        this.activatedRoute.queryParams.subscribe(async (params) => {
            if (params.id !== undefined) {
                this.his_id = params.id;
                console.log(this.his_id);
                this.him = params.pseudo;
                console.log(this.him);
                this.getMessages();
                /*const loading = await this.loadingController.create({
                  message: 'Chargement...'
                });
                loading.present();
                  this.firebase.getAlldataByField("users", "id", this.his_id).then(res => {
                      console.log(res);
                      loading.dismiss();
                      this.him = res.length > 0 ? res[0] : null;
                      console.log(this.him);
                      this.getMessages();
                  });*/
            }
        });
    }
    scrollToBottomOnInit() {
        console.log("hey scrolling");
        setTimeout(() => {
            if (this.content.scrollToBottom) {
                this.content.scrollToBottom(0);
            }
        }, 500);
    }

    uploadImage(imageURI){
        return new Promise<any>((resolve, reject) => {
            let storageRef = firebase.storage().ref();
            let c = new Date();
            c.getMilliseconds()
            let imageRef = storageRef.child('image').child(this.him+"_"+c.getMilliseconds()+".png");
            imageRef.putString(imageURI, 'base64')
                .then(snapshot => {
                    console.log(snapshot);
                    this.loading = "";
                    console.log(imageRef.getDownloadURL());
                    resolve(imageRef.getDownloadURL())
                }, err => {
                    reject(err);
                });
            /*this.encodeImageUri(imageURI, function(image64){
                imageRef.putString(image64, 'data_url')
                    .then(snapshot => {
                        resolve(snapshot.downloadURL)
                    }, err => {
                        reject(err);
                    })
            })*/
        })
    }


    async selectImage() {
        const actionSheet = await this.actionSheetCtr.create({
            header: "Choisir à partir de : ",
            buttons: [
                {
                    text: 'Galerie',
                    icon: 'image',
                    handler: () => {
                        this.openImagePicker(this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Caméra',
                    icon: 'camera',
                    handler: () => {
                        this.openImagePicker(this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Annuler',
                    icon: 'remove-circle',
                    role: 'cancel'
                }
            ]
        });
        await actionSheet.present();
    }

    openImagePicker(sourceType: PictureSourceType){
        this.options.sourceType = sourceType;
        const opt = this.options;
        if (sourceType == this.camera.PictureSourceType.CAMERA) {
            //opt.saveToPhotoAlbum = true;
            //opt.allowEdit = true;
        }
        this.camera.getPicture(opt).then((imageData) => {
            //this.myImage = 'data:image/jpeg;base64,' + imageData;
            console.log(imageData);
            this.loading = "Envoie en cours...";
            //console.log((<any>window).Ionic.WebView.convertFileSrc(imageData));
            this.uploadImageToFirebase(imageData);//(<any>window).Ionic.WebView.convertFileSrc(imageData));
            //this.imageData = imageData;
            //this.image = (<any>window).Ionic.WebView.convertFileSrc(imageData);
            //this.save();
            //console.log(this.image);
            //this.camera.cleanup();
            /*this.file.resolveLocalFilesystemUrl(imageData).then((entry: FileEntry) => {
                entry.file(file => {
                    console.log(file);

                    //this.formData.append('Photo', file);
                    this.readFile(file);
                });
            });*/
        }, (err) => {
            console.log(err);
            // Handle error
        });
        /*this.imagePicker.hasReadPermission().then(
            (result) => {
                if(result == false){
                    // no callbacks required as this opens a popup which returns async
                    this.imagePicker.requestReadPermission();
                }
                else if(result == true){
                    this.imagePicker.getPictures({
                        maximumImagesCount: 1
                    }).then(
                        (results) => {
                            for (var i = 0; i < results.length; i++) {
                                this.uploadImageToFirebase(results[i]);
                            }
                        }, (err) => console.log(err)
                    );
                }
            }, (err) => {
                console.log(err);
            });*/
    }
    uploadImageToFirebase(image){
        //image = normalizeURL(image);

        //uploads img to firebase storage
        this.uploadImage(image)
            .then(photoURL => {
                console.log(photoURL);
                this.msgVal = photoURL;
                this.chatSend(null, 'image');
                /*let toast = this.toastCtrl.create({
                    message: 'Image was updated successfully',
                    duration: 3000
                });
                toast.present();*/
            })
    }

    encodeImageUri(imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext("2d");
        var img = new Image();
        img.onload = function () {
            var aux:any = this;
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            var dataURL = c.toDataURL("image/jpeg");
            callback(dataURL);
        };
        img.src = imageUri;
    };

    getMessages() {
        this.afs.collection('chat', ref => ref.orderBy('date_stamp', 'asc')).valueChanges({idField: 'dataId'})
            .subscribe(snapshots => {
                var chats = [];
                snapshots.forEach((c: any) => {
                    if ((c.from_id == this.me.id && c.to_id == this.his_id) || (c.from_id == this.his_id && c.to_id == this.me.id)) {
                        chats.push(c);
                        if (c.to_id == this.me.id) {
                            this.afs.collection('chat').doc(c.dataId).update({
                                is_new: false
                            })
                                .then(
                                    res => {
                                        console.log("set viewed correctly");
                                    },
                                    err => {
                                        console.log("error setting viewed ", err);
                                    }
                                );
                        }
                    }
                });
                this.messages = chats;
                this.scrollToBottomOnInit();
                console.log(snapshots);
            }, err => {
                //reject(err);
            });


        /*this.firebase.getMessages(this.me, this.his_id).then(res => {
            this.messages = res;
        });*/
    }

    chatSend(theirMessage: string, type_) {
        let data = {
            to_id: this.his_id,
            to_name: this.him,
            from_name: this.me.pseudo,
            from_id: this.me.id,
            message: this.msgVal,
            type: type_,
            date: new Date().toLocaleString()
        };
        //this.messages.push(data);
        this.msgVal = '';
        this.fireb.sendMessage(data, null).then(res => {

        }, err => {
            this.fireb.creatToast(err);
        });
    }

}
