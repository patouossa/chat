// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyA0K0xg6cFVFyqdhnLtFfawHVCfkBvvAxQ",
    authDomain: "chat-13583.firebaseapp.com",
    databaseURL: "https://chat-13583.firebaseio.com",
    projectId: "chat-13583",
    storageBucket: "chat-13583.appspot.com",
    messagingSenderId: "940864872321",
    appId: "1:940864872321:web:f2796b20183f8b704b5280",
    measurementId: "G-1F5W4HEP3W"

  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
